<html>
	<head>
		<title>Ajax Query</title>

		<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">

		<script type="text/javascript" src="js/jquery.js"></script>

	</head>
	<head>
		
		Search by Ajax from Database <br/> <br/>

		<input type="text" name="search" id="search">

		<div id="showReault" style="color: red;">  Result : </div>

		<script type="text/javascript">
			
			$('#search').keyup( function(){

				var search = $('#search').val();

				$.ajax({
					url:'search.php',
					data:'searchvalu='+search,

					success:function(data)
					{
						$('#showReault').html(data);
					}
				});
			});
			
		</script>
		
	</head>
</html>